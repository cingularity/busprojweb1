import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// import { useQuery } from "@apollo/client";
// import { GET_ALL_USERS } from "./queries/users";
import { SignInPage, Home } from "./containers";

// type User = {
//   id: string;
//   username: string;
//   email: string;
// };

// function App() {
//   const { loading, error, data } = useQuery(GET_ALL_USERS);
//   if (loading) {
//     return <p>Loading ...</p>;
//   }
//   if (error) {
//     return <p>Error :</p>;
//   }

//   return data.users.map((user: User) => (
//     <div key={user.id}>
//       <p>Username: {user.username}</p>
//       <p>Email: {user.email}</p>
//     </div>
//   ));
// }

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/signin">
          <SignInPage />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
