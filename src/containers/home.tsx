import React, { Fragment } from "react";
import { Header, Footer } from "../layout";

const Home = () => (
  <Fragment>
    <Header />
    <div>Home</div>
    <Footer />
  </Fragment>
);

export default Home;
