import React, { Fragment } from "react";
import { SignIn } from "../components";
import { Header, Footer } from "../layout";

const SignInPage = () => (
  <Fragment>
    <Header />
    <SignIn />
    <Footer />
  </Fragment>
);

export default SignInPage;
