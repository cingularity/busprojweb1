import Cookies from 'js-cookie';

export const setToken = (token: string) => {
  Cookies.set('x-token', token, {expires: 1})
}

export const getToken = () => {
  return Cookies.get('x-token')
}