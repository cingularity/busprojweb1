import React, { useState, SyntheticEvent } from "react";
import { setToken } from "../cookies";
import { Container, Form, Button } from "semantic-ui-react";
import { useMutation } from "@apollo/client";
import { SIGN_IN } from "../operations/mutations/user";

interface Props {}

interface Login {
  login: string;
  password: string;
}

const SignIn: React.FC = (props: Props) => {
  const [signIn, { error, loading }] = useMutation(SIGN_IN);
  const [loginDetail, setLoginDetail] = useState<Login>({
    login: "",
    password: "",
  });

  const handleChange = (name: string, value: string) => {
    setLoginDetail((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSubmit = async (e: SyntheticEvent) => {
    e.preventDefault();
    const { data } = await signIn({ variables: { ...loginDetail } });
    const { token } = data.signIn;
    setToken(token);
  };

  if (loading) return <p>Loading...</p>;
  if (error) {
    return <p>{error.message}</p>;
  }

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <Form.Input
          name="login"
          placeholder={"username or email"}
          required
          value={loginDetail.login}
          onChange={(e, { name, value }) => handleChange(name, value)}
        />
        <Form.Input
          name="password"
          required
          placeholder={"password"}
          value={loginDetail.password}
          onChange={(e, { name, value }) => handleChange(name, value)}
        />
        <Button negative type="button" content={"cancel"} />
        <Button positive type="submit" content="Submit" />
      </Form>
    </Container>
  );
};

export default SignIn;
